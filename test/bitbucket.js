var assert = require("assert");
var chai = require("chai");
var expect = chai.expect;
var colors = require("colors");

try 
{ 
    var settings = require(__dirname+"/settings.json"); 
    console.log("File loaded succesfully".green)
} 
catch (e) 
{ 
    console.error('Create test/settings.json and populate with your credentials.'.red);
}

try 
{
    var Bitbucket = require(__dirname+'/../bitbucket.js').BitbucketApi;
    console.log("Bitbucket loaded successfully".green)

}

catch (e) 
{ 
    console.error('Failed to load the bitbucket.'.red);
}

 describe('Creating bitbucket object', function(){
  describe('bitbucket', function(){
    bitbucket = new Bitbucket(settings.apikey, settings.endpoint);
    it('should exists and be an object', function(){
      expect(bitbucket).to.exist;
      expect(bitbucket).to.be.an('object');
      expect(bitbucket).to.be.instanceof(Bitbucket);
      
    });
  });
});

 