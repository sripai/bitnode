var url = require('url'),
    logger = console,
    OAuth = require("oauth");

    var BitbucketApi = exports.BitbucketApi = function(protocol, host, username, password, apiVersion, verbose, strictSSL, oauth) {
    this.protocol = protocol;
    this.host = host;
    this.username = username;
    this.password = password;
    this.apiVersion = apiVersion;
    // Default strictSSL to true (previous behavior) but now allow it to be
    // modified
    if (strictSSL == null) {
        strictSSL = true;
    }
    this.strictSSL = strictSSL;
    // This is so we can fake during unit tests
    this.request = require('request');
    if (verbose !== true) { logger = { log: function() {} }; }

    // This is the same almost every time, refactored to make changing it
    // later, easier
    this.makeUri = function(pathname, altBase, altApiVersion) {
        var basePath = '/api/';
        if (altBase != null) {
            basePath = altBase;
        }

        var apiVersion = this.apiVersion;
        if (altApiVersion != null) {
          apiVersion = altApiVersion;
        }

        var uri = url.format({
            protocol: this.protocol,
            hostname: this.host,
            port: this.port,
            pathname: basePath + apiVersion + pathname
        });
        return decodeURIComponent(uri);
    };

    this.doRequest = function(options, callback) {
        if(oauth && oauth.consumer_key && oauth.consumer_secret) {
          options.oauth = {
            consumer_key: oauth.consumer_key,
            consumer_secret: oauth.consumer_secret,
            token: oauth.access_token,
            token_secret: oauth.access_token_secret
          };
        } else {
          options.auth = {
            'user': this.username,
            'pass': this.password
          };
        }
        this.request(options, callback);
    };

};

(function(){
    //API methods to get the user details
    this.getUserDetails = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoops!! Looks like you have hit a no man's land.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
                return;
            }

            if (response.statusCode === 403) {
                callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    //Lists the privileges for the user

    this.getUserPrivileges = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/privileges/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('User not found');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            if (response.statusCode === 403) {
                callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
                return;
            }

            callback(null, body);

        });
    };



    this.getUserFollows = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/follows/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("It seems like you have hit a no man's land or else the user is not found");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.getUserRepos = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/repositories/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('Repository not found');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (response.statusCode === 403) {
                callback('Unauthorised to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.getUserRepoOverview = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/repositories/overview/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 403) {
                callback('Unauthorised to connect to Bitbucket ');
                return;
            }

            if (response.statusCode === 404) {
                callback('User not found');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.getUserRepoDashboard = function(callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/user/repositories/dashboard/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('User not found');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            if (response.statusCode === 403) {
                callback('User not found');
                return;
            }

            callback(null, body);

        });
    };

    // API methods to get the repositories

    this.getRepositoryDetails = function(accountname,reponame,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountname+'/'+reponame),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa, looks like you have landed in no man's land");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (response.statusCode === 403) {
                callback('Needs security Credentials');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.createRepository = function(name,description,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "name": name

            },
        };

        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa looks like you have landed in no man's land");
                return;
            }

            if (response.statusCode === 403) {
                callback('Needs security Credentials');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,response)

        });
    };

    this.deleteRepository = function(accountname,reponame,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountname+'/'+reponame),
            method: 'DELETE',
            json: true

        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }

            callback(response.statusCode + ': Error while deleting Repository');


        });
    };


    this.getRepositoryLinks = function(accountname,reponame,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountname+'/'+reponame+'/links'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('User not found');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (response.statusCode === 403) {
                callback('Needs security Credentials');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };


    this.createRepositoryLink = function(accountName,repoName, handler,link_url,link_key,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountName+'/'+repoName+'/links'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "handler": handler,
              "link_url":link_url,
              "link_key":link_key
            },
        };

        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback('The currently authenticated user does not have permission.');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createRepositoryLink.');
                return;
            }


            callback(null,response)

        });
    };

    this.updateRepositoryLink = function(accountName,repoName, handler,link_url,link_key,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountName+'/'+repoName+'/links'),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            body: {
              "handler": handler,
              "link_url":link_url,
              "link_key":link_key
            },
        };

        this.doRequest(options, function(error, response) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 200 || response.statusCode === 204) {
                callback(null, "Success");
                return;
            }

            callback(response.statusCode + ': Error while updating');

        });

    };

    this.deleteRepositoryLink = function(accountname,reponame,linkId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountname+'/'+reponame),
            method: 'DELETE',
            json: true,
            form: {
                "link_id":linkId
            }

        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }

            callback(response.statusCode + ': Error while deleting repository link.');


        });
    };

    this.getRepositoryDeployKeys = function(accountname,reponame,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountname+'/'+reponame+'/deploy-keys'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.createRepositoryDeploylink = function(accountName,repoName, key,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountName+'/'+repoName+'/deploy-keys'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "key": key


            },
        };

        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createRepositoryDeploylink.');
                return;
            }

            if(body===undefined) {
                callback('Response body was undefined.');

            }


            callback(null,response)

        });
    };

    this.deleteRepositoryDeploylink = function(accountName,repoName,key,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountName+'/'+repoName+'/deploy-keys'+'/'+key),
            method: 'DELETE',
            followAllRedirects: true,
            json: true

        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }

            callback(response.statusCode + ': Error while deleting deploy keys');


        });
    };

    this.createRepositoryFork = function(accountName,repoName, forkName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories'+'/'+accountName+'/'+repoName+'/fork'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "name": forkName


            },
        };

        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,response)

        });
    };
    this.getUsers = function(accountname,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.deleteUsers = function(accountname,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname),
            method: 'DELETE',
            json:true
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }

            callback(response.statusCode + ': Error while deleting deleting the users.');


        });

    };

    this.getIssues = function(accountname,repoName, callback) {



        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repoName+'/issues/'),
            method: 'GET'

        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket ');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.createIssue = function(accountName, repoName, title,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "title":title

            }

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Version does not exist or the currently authenticated user does not have permission to view it");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createIssue.');
                return;
            }


            callback(null,body)


        });
    };

    this.getIssueDetails = function(accountName, repoName,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId),
            method: 'GET',
            followAllRedirects: true,
            json: true

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during getIssueDetails.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateIssueDetails = function(accountName, repoName,title,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "title":title

            }

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during updateIssueDetails.');
                return;
            }

            if(body===undefined) {
                callback("Response body was undefined");

            }


            callback(null,body)


        });
    };

    this.deleteIssue = function(accountName, repoName,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 204) {
                callback(response.statusCode + ': Unable to delete the issue');
                return;
            }


            callback(null,"Successfully deleted the issue")


        });
    };

    this.getIssueFollowers = function(accountName, repoName,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId+'/followers/'),
            method: 'GET',
            followAllRedirects: true,
            json: true

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }

            if(body===undefined) {


            }


            callback(null,body)


        });
    };

    this.getIssueComments = function(accountName, repoName,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId+'/comments/'),
            method: 'GET',
            followAllRedirects: true,
            json: true

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addComment = function(accountName,repoName,comment,issueId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId+'/comments'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "content": comment

            },
        };


        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)

        });
    };

    this.getComment = function(accountName, repoName,issueId,commentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId+'/comments/'+commentId+'/'),
            method: 'GET',
            followAllRedirects: true,
            json: true

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateComment = function(accountName, repoName,issueId,commentId,comment,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId+'/comments/'+commentId+'/'),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "content":comment

            }

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };
    this.getComponents = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/components/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addComponent = function(accountName, repoName,name,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/components/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "name":name

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getComponent = function(accountName, repoName,componentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/components/'+componentId),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateComponent = function(accountName, repoName,componentId,newComponent,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/components/'+componentId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "name": newComponent

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deleteComponent = function(accountName, repoName,componentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/components/'+componentId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }


            callback(null,"Error deleting the component")


        });
    };

    this.getMilestones = function(accountName,repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/milestones/'),
            method: 'GET',
            followAllRedirects: true,
            json: true


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addMilestone = function(accountName, repoName,name,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/milestones/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "name":name

            }


        };



        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };


    this.getMilestone = function(accountName,repoName,milestoneId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug +'/issues/milestones/'+milestoneId+'/'),
            method: 'GET',
            json:true
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if(response.statusCode === 403) {
                callback("Not authenticated to use the API")

            }

            if (response.statusCode === 404) {
                callback('Invalid issue number.');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during findIssueStatus.');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.updateMilestoneDetails = function(accountName, repoName,name,milestoneId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/'+issueId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "name":name

            }

        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deleteMilestone = function(accountName, repoName,milestoneId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/milestones/'+milestoneId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }


            callback(null,"Error deleting the component")


        });
    };

    this.getVersions = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/versions/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addVersion = function(accountName, repoName,name,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/versions/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "name":name

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getVersion = function(accountName, repoName,versionId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/versions/'+versionId),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateVersion = function(accountName, repoName,versionId,newVersion,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/versions/'+versionId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "name": newVersion

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deleteVersion = function(accountName, repoName,versionId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/issues/versions/'+versionId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 204) {
                callback(response.statusCode + ': Unable to delete the issue');
                return;
            }


            callback(null,"Successfully deleted the issue")


        });
    };
    this.getRepositoryTags = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/tags/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryBranches = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/branches/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryMainBranch = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/main-branch/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryWiki = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/wiki/page/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.createRepositoryWiki = function(accountName, repoName,wikiContent,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/wiki/page/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "content":wikiContent
            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateRepositoryWiki = function(accountName, repoName,wikiContent,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/wiki/page/'),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "content":wikiContent
            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getpullRequestComments = function(accountName, repoName,pullrequestId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getpullRequestComment = function(accountName, repoName,pullrequestId,commentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'+commentId+'/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getpullRequestComment = function(accountName, repoName,pullrequestId,commentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'+commentId+'/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addPullrequestComment = function(accountName,repoName,pullrequestId,name,description,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
              "content": name

            },
        };

        this.doRequest(options, function(error, response, body) {



            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,response)





        });
    };

    this.updatePullrequestComment = function(accountName, repoName,pullrequestId,commentId,updatedComment,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'+commentId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "name": updatedComment

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deletePullrequestComment = function(accountName, repoName,pullrequestId,commentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/pullrequests/'+pullrequestId+'/comments/'+commentId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.spamPullRequestComment = function(accountName, repoName,issueId,commentId,comment,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug+'/pullrequests/'+pull_request_id+'/comments/spam/'+comment_id),
            method: 'PUT',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getChangerequestParticipants = function(accountName, repoName,requestId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug+'/pullrequests/'+requestId+'/participants/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRevisionSource = function(accountName, repoName,revision,path,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug+'/src/'+requestId+revision+path),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRevisionRawSource = function(accountName, repoName,revision,path,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug+'/raw/'+requestId+revision+path),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryEvents = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountname+'/'+repo_slug+'/events/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getUserEvents = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname+'/'+repo_slug+'/events/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getUserEvents = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname+'/'+repo_slug+'/events/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryEvents = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname+'/events/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getRepositoryServices = function(accountName, repoName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname+repoName+'/services/'),
            method: 'GET',
            followAllRedirects: true,
            json: true,


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.addRepositoryServices = function(accountName, repoName,type,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountname+repoName+'/services/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "type":type

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.updateRepositoryService = function(accountName, repoName,componentId,newService,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/services/'+serviceId),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "type": newService

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deleteRepositoryServce = function(accountName, repoName,serviceId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/services/'+serviceId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }


            callback(null,"Error deleting the service")


        });
    };

    this.createPatchQueue = function(accountName, repoName,name,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/repositories/'+accountName+'/'+repoName+'/mq/'),
            method: 'POST',
            followAllRedirects: true,
            json: true,
            form: {
                "name":name

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.getAccountPlan = function(accountName,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/users/'+accountName+'/plan/'),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('Invalid issue number.');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during findIssueStatus.');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.getPrivilegedGroups = function(accountName,permission,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/group-privileges/'+accountName+'?filter='+permission),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('Invalid issue number.');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during findIssueStatus.');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.getPrivilegedGroupRepo = function(accountName,groupOwner,groupId,permission,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/group-privileges/'+accountName+groupOwner+'/'+groupId+'?filter='+permission),
            method: 'GET'
        };

        this.doRequest(options, function(error, response, body) {

            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback('Invalid issue number.');
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during findIssueStatus.');
                return;
            }

            if (body === undefined) {
                callback('Response body was undefined.');
            }

            callback(null, body);

        });
    };

    this.deleteGroupPermission = function(accountName, repoName,componentId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/group-privileges/'+accountName+groupOwner+'/'+groupId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }


            callback(null,"Error deleting the component")


        });
    };

    this.updateGroupRepoPermission = function(accountName, repoName,componentId,privilege,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/group-privileges/'+accountname+'/'+repo_slug+'/'+group_owner+'/'+group_slug),
            method: 'PUT',
            followAllRedirects: true,
            json: true,
            form: {
                "privilege": privilege

            }


        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 404) {
                callback("Whoa it looks like you have landed in no man's land.");
                return;
            }

            if (response.statusCode === 403) {
                callback("The currently authenticated user does not have permission.Needs security Credentials.");
                return;
            }

            if (response.statusCode !== 200) {
                callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
                return;
            }


            callback(null,body)


        });
    };

    this.deleteGroupPermission = function(accountName, repoName,groupOwner,groupId,callback) {

        var options = {
            rejectUnauthorized: this.strictSSL,
            uri: this.makeUri('/group-privileges/'+accountName+'/'+repoName+'/'+groupOwner+'/'+groupId),
            method: 'DELETE',
            followAllRedirects: true,
            json: true,



        };

        this.doRequest(options, function(error, response, body) {


            if (error) {
                callback(error, null);
                return;
            }

            if (response.statusCode === 204) {
                callback(null, "Success");
                return;
            }


            callback(null,"Error deleting the component")


        });
    };
//Get team permissions
//     this.getTeamPrivileges = function(accountName,callback) {
//             var options = {
//             rejectUnauthorized: this.strictSSL,
//             uri: this.makeUri('/users/'+accountName+'/privileges/'),
//             method: 'GET',
//             followAllRedirects: true,
//             json: true,
//
//
//
//         };
//
//         this.doRequest(options, function(error, response, body) {
//
//             if (error) {
//                 callback(error, null);
//                 return;
//             }
//
//             if (response.statusCode === 404) {
//                 callback("Whoops!! Looks like you have hit a no man's land.");
//                 return;
//             }
//
//             if (response.statusCode !== 200) {
//                 callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
//                 return;
//             }
//
//             if (response.statusCode === 403) {
//                 callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
//                 return;
//             }
//
//             if (body === undefined) {
//                 callback('Response body was undefined.');
//             }
//
//             callback(null, body);
//
//         });
//
//     };
//
// this.getuserPrivilegesinTeam = function(accountName,groupOwner,groupSlug,callback) {
//             var options = {
//             rejectUnauthorized: this.strictSSL,
//             uri: this.makeUri('/users/'+accountName+'/privileges/'+groupOwner+'/'+groupSlug+'/'),
//             method: 'GET',
//             followAllRedirects: true,
//             json: true,
//
//
//
//         };
//
//         this.doRequest(options, function(error, response, body) {
//
//             if (error) {
//                 callback(error, null);
//                 return;
//             }
//
//             if (response.statusCode === 404) {
//                 callback("Whoops!! Looks like you have hit a no man's land.");
//                 return;
//             }
//
//             if (response.statusCode !== 200) {
//                 callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
//                 return;
//             }
//
//             if (response.statusCode === 403) {
//                 callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
//                 return;
//             }
//
//             if (body === undefined) {
//                 callback('Response body was undefined.');
//             }
//
//             callback(null, body);
//
//         });
//
//     };
//
//     this.createUserPermissions(accountName,groupOwner,groupSlug,privileges) {
//       var options = {
//       rejectUnauthorized: this.strictSSL,
//       uri: this.makeUri('/users/'+accountName+'/privileges/'+groupOwner+'/'+groupSlug+'/'),
//       method: 'POST',
//       followAllRedirects: true,
//       json: true,
//       form: {
//         "privileges": privileges
//
//       },
//
//
//
//   };
//
//   this.doRequest(options, function(error, response, body) {
//
//
//
//       if (error) {
//           callback(error, null);
//           return;
//       }
//
//       if (response.statusCode === 404) {
//           callback("Whoa looks like you have landed in no man's land");
//           return;
//       }
//
//       if (response.statusCode === 403) {
//           callback('Needs security Credentials');
//           return;
//       }
//
//       if (response.statusCode !== 200) {
//           callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
//           return;
//       }
//
//
//       callback(null,response)
//
//   });
//
// }
//
//
// this.updateUserPermissions(accountName,groupOwner,groupSlug,privileges) {
//   var options = {
//   rejectUnauthorized: this.strictSSL,
//   uri: this.makeUri('/users/'+accountName+'/privileges/'+groupOwner+'/'+groupSlug+'/'),
//   method: 'PUT',
//   followAllRedirects: true,
//   json: true,
//   form: {
//     "privileges": privileges
//
//   },
//
//
//
// };
//
//   this.doRequest(options, function(error, response) {
//
//       if (error) {
//           callback(error, null);
//           return;
//       }
//
//       if (response.statusCode === 200 || response.statusCode === 204) {
//           callback(null, "Success");
//           return;
//       }
//
//       callback(response.statusCode + ': Error while updating');
//
//   });
//
// this.deleteUserPermissions(accountName,groupOwner,groupSlug,privileges) {
//   var options = {
//   rejectUnauthorized: this.strictSSL,
//   uri: this.makeUri('/users/'+accountName+'/privileges/'+groupOwner+'/'+groupSlug+'/'),
//   method: 'DELETE',
//   followAllRedirects: true,
//   json: true,
//   form: {
//     "privileges": privileges
//
//   },
//
//
//
// };
//
//     this.doRequest(options, function(error, response, body) {
//
//         if (error) {
//             callback(error, null);
//             return;
//         }
//
//         if (response.statusCode === 204) {
//             callback(null, "Success");
//             return;
//         }
//
//         callback(response.statusCode + ': Error while deleting Repository');
//
//
//     });
//
//
// };

this.getGroups = function(accountName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName),
        method: 'GET'
    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};

this.createGroup = function(accountName,groupName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'),
        method: 'POST',
        followAllRedirects: true,
        json: true,
        form: {
          "name": groupName

        },
    };

    this.doRequest(options, function(error, response, body) {



        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoa looks like you have landed in no man's land");
            return;
        }

        if (response.statusCode === 403) {
            callback('Needs security Credentials');
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during createVersion.');
            return;
        }


        callback(null,response)

    });
};

this.getGroupDetails = function(accountName,groupName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName+'/'),
        method: 'GET',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};

this.deleteGroup = function(accountName,groupName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName+'/'),
        method: 'DELETE',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 204) {
            callback(null, "Success");
            return;
        }

        callback(response.statusCode + ': Error while deleting Repository');


    });


};

this.updateGroup = function(accountName,groupName,newGroupname,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName),
        method: 'PUT',
        followAllRedirects: true,
        json: true,
        body: {
          "name": newGroupname,

        },
    };

    this.doRequest(options, function(error, response) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 200 || response.statusCode === 204) {
            callback(null, "Success");
            return;
        }

        callback(response.statusCode + ': Error while updating');

    });

};


this.getGroupMembers = function(accountName,groupName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName+'/members/'),
        method: 'GET',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};


this.getGroupMemberAccount = function(accountName,groupName,memberAccount,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName+'/members/'+memberAccount),
        method: 'GET',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};


this.updateGroupMemberAccount = function(accountName,groupName,memberAccount,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountName+'/'+groupName+'/members/'+memberAccount),
        method: 'PUT',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 200 || response.statusCode === 204) {
            callback(null, "Success");
            return;
        }

        callback(response.statusCode + ': Error while updating');

    });
};


this.deleteGroupMemberAccount = function(accountname,groupName,memberAccount,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/groups/'+accountname+'/'+groupName+'/members/'+memberAccount),
        method: 'DELETE',
        json: true

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 204) {
            callback(null, "Success");
            return;
        }

        callback(response.statusCode + ': Error while deleting Repository');


    });
};



this.getFollowers = function(accountName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/users/'+accountName+'/followers/'),
        method: 'GET',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};


this.getRepoFollowers = function(accountName,repoName,callback) {

    var options = {
        rejectUnauthorized: this.strictSSL,
        uri: this.makeUri('/repositories/'+accountName+repoName+'/followers/'),
        method: 'GET',
        followAllRedirects: true,
        json: true,

    };

    this.doRequest(options, function(error, response, body) {

        if (error) {
            callback(error, null);
            return;
        }

        if (response.statusCode === 404) {
            callback("Whoops!! Looks like you have hit a no man's land.");
            return;
        }

        if (response.statusCode !== 200) {
            callback(response.statusCode + ': Unable to connect to Bitbucket during getUserDetails.');
            return;
        }

        if (response.statusCode === 403) {
            callback("Looks like you are unauthorised to make this API call, please check your credentials and retry");
            return;
        }

        if (body === undefined) {
            callback('Response body was undefined.');
        }

        callback(null, body);

    });
};


}









}).call(BitbucketApi.prototype)

var bitbucket = new BitbucketApi('https', 'bitbucket.org', "sripathi.pai@spartez.com", "Sudhamrith@1", '1.0');
bitbucket.getGroupMembers("sripai","update",function(error, issue) {
         console.log('Privileges: ' + issue);

     });
